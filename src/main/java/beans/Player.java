package beans;


import java.util.StringJoiner;

// Java Bean (POJO = plain ol' java object)
public class Player {

    private Integer id;
    private Integer score;
    private String name;

    public Player(String name, int score, int userId) {
        this.id = id;
        this.score = score;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Player{");
        sb.append("id=").append(id);
        sb.append(", score=").append(score);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}