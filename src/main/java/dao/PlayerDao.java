package dao;

import beans.Player;
import com.mysql.cj.jdbc.DatabaseMetaData;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Component
public class PlayerDao {

    private static DataSource dataSource;
    // @Autowired
//    private DataSource dataSource;

    /*
    public UserDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    */

    public DataSource getDataSource() {
        return dataSource;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static Player findById(Integer id) {
        // exec SQL query pentru gasirea userului cu id-ul primit

        try (Connection con = dataSource.getConnection()) {
            PreparedStatement query = con.prepareStatement("select * from player where id = ?");
            query.setInt(1, id);
            final ResultSet resultSet = query.executeQuery();

            // while (conditie) {
            //     instructiuni;
            // }

            // do {
            //     instructiuni
            // }
            // while (conditie)

            // for (int i = 0; i < N; i++) {
            //     instrcutiuni
            // }

            // for (E e : colectie) {
            //    instructiuni
            // }

            if (resultSet.next()) {
                final int userId = resultSet.getInt("id");
                final int score = resultSet.getInt("score");
                String name = resultSet.getString("name");

                return new Player(name, score, userId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public Player findByUsername(String username) {

        try (Connection con = dataSource.getConnection()) {
            PreparedStatement query = con.prepareStatement("select * from player where name = ?");
            query.setString(1, username);
            final ResultSet resultSet = query.executeQuery();

            if (resultSet.next()) {
                return new Player(resultSet.getString("name"), resultSet.getInt("score"), resultSet.getInt("id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
