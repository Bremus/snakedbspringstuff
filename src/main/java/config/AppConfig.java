package config;

import com.mysql.cj.jdbc.MysqlDataSource;
import dao.PlayerDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@ComponentScan("dao")
public class AppConfig {

    @Bean
    public DataSource getDataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL("jdbc:mysql://localhost:3306/snake");
        dataSource.setUser("root");
        dataSource.setPassword("1234");
        return dataSource;
    }

    /*
    @Bean
    public UserDao getUserDao() {
        return new UserDao(getDataSource());
    }
    */
}
