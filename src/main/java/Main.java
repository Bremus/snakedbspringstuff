import beans.Player;
// import config.Application;
import config.AppConfig;
import dao.PlayerDao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {

        // ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        // final DataSource dataSource = (DataSource) context.getBean("dataSource");
        // final DataSource dataSource = (DataSource) context.getBean(DataSource.class);

        // UserDao userDao = new UserDao(Application.INSTANCE.getDataSource());
        // UserDao userDao = new UserDao(dataSource);
        PlayerDao userDao = context.getBean(PlayerDao.class);

        final Player user = PlayerDao.findById(1);
        System.out.println(user);

        final Player byUsername = userDao.findByUsername("ionut");
        System.out.println(byUsername);

    }
}
